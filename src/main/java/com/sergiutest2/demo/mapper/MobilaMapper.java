package com.sergiutest2.demo.mapper;

import com.sergiutest2.demo.Dto.MobilaDto;
import com.sergiutest2.demo.Entity.Mobila;


public class MobilaMapper {

    public static Mobila mobilaDtoToEntity(MobilaDto mobila) {
        return new Mobila().setPat(mobila.getName())
                .setId(mobila.getId())
                .setMasa(mobila.getName())
                .setBirou(mobila.getName())
                .setDulap(mobila.getName());
    }
}

