package com.sergiutest2.demo.service;

import com.sergiutest2.demo.Dto.MobilaDto;
import com.sergiutest2.demo.Entity.Mobila;
import com.sergiutest2.demo.Repository.MobilaRep;

import com.sergiutest2.demo.mapper.MobilaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MobilaService {
    @Autowired
    MobilaRep mobilaRep;

    public List<Mobila> getAllMobila(){
        return mobilaRep.findAll();
    }
    public Optional<Mobila> findById(long id) {
        return mobilaRep.findById(id);
    }

    public Mobila saveMobila(Mobila mobila) {
        return mobilaRep.save(mobila);
    }

    public Mobila getOneMobila(Long id) {
        return mobilaRep.getOne(id);
    }
    public String getDulap(Mobila mobila) {
        return mobila.getDulap();
    }

   public Long addMobila(MobilaRep mobilaRep,Long id ) {

        mobilaRep.findById(id).isPresent();
        return id;


  }
}

