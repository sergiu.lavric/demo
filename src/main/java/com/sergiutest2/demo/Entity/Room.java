//package com.sergiutest2.demo.Entity;
//
//import org.springframework.beans.factory.annotation.Autowired;
//
//import javax.persistence.*;
//import java.util.List;
//
//@Entity
//public class Room {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    private Integer pereti;
//
//
//    @OneToMany(mappedBy = "camera", cascade = CascadeType.ALL)
//    private List<Mobila> mobila;
//
//    @Autowired
//    public Room() {
//
//    }
//
//    public Room(Integer pereti) {
//        this.pereti = pereti;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public Integer getPereti() {
//        return pereti;
//    }
//
//    public void setPereti(Integer pereti) {
//        this.pereti = pereti;
//    }
//
//    public List<Mobila> getMobila() {
//        return mobila;
//    }
//
//    public void setMobila(List<Mobila> mobila) {
//        this.mobila = mobila;
//    }
//}
//
