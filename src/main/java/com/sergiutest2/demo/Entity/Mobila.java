package com.sergiutest2.demo.Entity;
import lombok.NoArgsConstructor;
import javax.persistence.*;


@NoArgsConstructor
@Entity
@Table(name = "mobila")
public class Mobila {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "birou")
    private String birou;
    @Column(name = "dulap")
    private String dulap;
    @Column(name = "masa")
    private String masa;
    @Column(name = "pat")
    private String pat;

    public Long getId() {
        return id;
    }

    public Mobila setId(Long id) {
       return new Mobila().setId(id);
    }

    public String getBirou() {
        return birou;
    }

    public Mobila setBirou(String birou) {
        return  new Mobila().setBirou("birou") ;
    }

    public String getDulap() {
        return dulap;
    }

    public Mobila setDulap(String dulap) {
        return new Mobila().setDulap("dulap");
    }

    public String getMasa() {
        return masa;
    }

    public Mobila setMasa(String masa) {
        return new Mobila().setMasa("masa");
    }

    public String getPat() {
        return pat;
    }

    public Mobila setPat(String pat) {
        return new Mobila().setPat("pat");
    }
}








