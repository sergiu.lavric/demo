//package com.sergiutest2.demo.Repository;
//
//import com.sergiutest2.demo.Entity.Room;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.util.Optional;
//
//public interface CameraRep  extends JpaRepository<Room,String> {
//    Optional<Room> findAllById(Long id);
//}
