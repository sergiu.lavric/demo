package com.sergiutest2.demo.Repository;
import com.sergiutest2.demo.Entity.Mobila;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MobilaRep extends JpaRepository<Mobila, Long> {
   List <Mobila>  findAllById(long id);



}
