package com.sergiutest2.demo.Controler;


import com.sergiutest2.demo.Dto.MobilaDto;
import com.sergiutest2.demo.Entity.Mobila;

import com.sergiutest2.demo.mapper.MobilaMapper;
import com.sergiutest2.demo.service.MobilaService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.*;

@RestController
@RequestMapping("/mobila")
public class MobilaControler {

    @Autowired
    MobilaService mobilaService;
     ;

    @GetMapping(value ="/get-all")
    public List<Mobila> getAll() {
        return mobilaService.getAllMobila();
    }

    @PutMapping(value = "/put")
public ResponseEntity<?> addMobila(MobilaDto mobilaDto) {
        Mobila mobila= MobilaMapper.mobilaDtoToEntity(mobilaDto);
        Mobila addMobila =mobilaService.saveMobila(mobila);
        URI locatie = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(addMobila.getId())
                .toUri();
        return ResponseEntity.created(locatie).build();
    }



}
